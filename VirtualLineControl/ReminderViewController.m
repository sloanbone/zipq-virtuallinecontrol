//
//  ReminderViewController.m
//  VirtualLineControl
//
//  Created by John Sloan on 8/17/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import "ReminderViewController.h"
#import "AppDelegate.h"

@interface ReminderViewController ()

@end

@implementation ReminderViewController

@synthesize panel_id;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    doneButton.layer.cornerRadius = 5;
    doneButton.layer.borderWidth = 1.0f;
    doneButton.layer.borderColor = [UIColor whiteColor].CGColor;
 
    num_0.layer.cornerRadius = num_0.frame.size.width/2;
    num_1.layer.cornerRadius = num_0.frame.size.width/2;
    num_2.layer.cornerRadius = num_0.frame.size.width/2;
    num_3.layer.cornerRadius = num_0.frame.size.width/2;
    num_4.layer.cornerRadius = num_0.frame.size.width/2;
    num_5.layer.cornerRadius = num_0.frame.size.width/2;
    num_6.layer.cornerRadius = num_0.frame.size.width/2;
    num_7.layer.cornerRadius = num_0.frame.size.width/2;
    num_8.layer.cornerRadius = num_0.frame.size.width/2;
    num_9.layer.cornerRadius = num_0.frame.size.width/2;
    
    numberString = [[NSMutableString alloc] init];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressNumber:(id)sender {
    
    numberString = [numberString stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)[sender tag]]];
    
    phoneNumber.text = numberString;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self.view endEditing:YES];

    return YES;
    
}

- (IBAction)clearText:(id)sender {
    
    phoneNumber.text = @"";
    numberString = @"";
    
}


-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    return NO;
}

- (IBAction)done:(id)sender {
    
    if([phoneNumber.text isEqualToString:@""]) {
        
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You must enter a phone number for reminders." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [a show];
        
        return;
        
    }

    if(appDelegate.internetIsReachable) {
    
        // send this to the server.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSData* data = [NSData dataWithContentsOfURL:
                        [NSURL URLWithString:[NSString stringWithFormat:@"http://register.growtix.com/mobile/setReminder/%@/%@",phoneNumber.text,panel_id]]];
        });
        
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"You will be reminded 15 minutes before the panel or event. Thanks!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [a show];
        
    } else {
        
        [appDelegate noInternet];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
 
    if (buttonIndex==0) {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    
}

- (IBAction)cancel:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
