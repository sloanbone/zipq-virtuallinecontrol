//
//  ReminderViewController.h
//  VirtualLineControl
//
//  Created by John Sloan on 8/17/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@interface ReminderViewController : UIViewController <UITextViewDelegate, UIAlertViewDelegate> {
    
    __weak IBOutlet UITextField *phoneNumber;
    __weak IBOutlet UIButton *doneButton;
    NSString *numberString;
    __weak IBOutlet UIButton *num_1;
    __weak IBOutlet UIButton *num_2;
    __weak IBOutlet UIButton *num_3;
    __weak IBOutlet UIButton *num_4;
    __weak IBOutlet UIButton *num_5;
    __weak IBOutlet UIButton *num_6;
    __weak IBOutlet UIButton *num_7;
    __weak IBOutlet UIButton *num_8;
    __weak IBOutlet UIButton *num_9;
    __weak IBOutlet UIButton *num_0;
    AppDelegate *appDelegate;
    
    NSMutableString *panel_id;
    
    
}

@property (nonatomic, strong) NSMutableString *panel_id;

@end
