//
//  TapViewController.m
//  VirtualLineControl
//
//  Created by John Sloan on 8/17/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import "TapViewController.h"
#import "AppDelegate.h"
#import "ReminderViewController.h"

@interface TapViewController ()

@end

@implementation TapViewController

#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    beaconPulse.layer.cornerRadius = beaconPulse.frame.size.width/2;
    cancelButton.layer.borderWidth = 1.0f;
    cancelButton.layer.borderColor = [UIColor whiteColor].CGColor;
    cancelButton.layer.cornerRadius = 5;
    
    remindMe.layer.borderWidth = 1.0f;
    remindMe.layer.borderColor = [UIColor whiteColor].CGColor;
    remindMe.layer.cornerRadius = 5;
    
    timerView.layer.cornerRadius = timerView.frame.size.width/2;
    successView.layer.cornerRadius = successView.frame.size.width/2;
    failView.layer.cornerRadius = failView.frame.size.width/2;
    
    steps = [[NSMutableArray alloc] init];
    
    [steps addObject:step1];
    [steps addObject:step2];
    [steps addObject:step3];
    [steps addObject:step4];
    
    [RFIDInput becomeFirstResponder];
    
    current_step = 0;
    
    KEY_USED = NO;
    
    [self animateBeacon];
    
}

- (void)textFieldDidBeginEditing:(UITextField*)textField
{
    if(SYSTEM_VERSION_GREATER_THAN(@"8.4")){
        UITextInputAssistantItem* item = [textField inputAssistantItem];
        item.leadingBarButtonGroups = @[];
        item.trailingBarButtonGroups = @[];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) animateBeacon {
    
    pulse = [[UIView alloc] initWithFrame:CGRectMake(beaconPulse.frame.origin.x, beaconPulse.frame.origin.y, beaconPulse.frame.size.width, beaconPulse.frame.size.height)];
    
    [pulse setBackgroundColor:[UIColor colorWithRed:85.0f/255.0f green:190.0f/255.0f blue:50.0f/255.0f alpha:1.0f]];
    pulse.layer.cornerRadius = pulse.frame.size.width/2;
    [pulse setCenter:CGPointMake(beaconPulse.center.x, beaconPulse.center.y)];
    
    pulse.alpha = 1.0;
    
    pulse.transform = CGAffineTransformMakeScale(0.0, 0.0);
    
    [step1 addSubview:pulse];
    //[self.view sendSubviewToBack:pulse];
    
    [step1 bringSubviewToFront:beaconPulse];
    
    [UIView animateWithDuration:1.5f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        
        pulse.alpha = 0.6;
        pulse.transform = CGAffineTransformMakeScale(1.4, 1.4);
        
    } completion: ^(BOOL complete) {
        
        [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            
            pulse.alpha = 0.0;
            pulse.transform = CGAffineTransformMakeScale(1.65, 1.65);
            
        } completion: ^(BOOL complete) {
            
            [self performSelector:@selector(animateBeacon) withObject:nil afterDelay:0.1];
            [pulse removeFromSuperview];
            pulse = nil;
            
        }];
        
    }];
    
    
}

- (IBAction)remindMe:(id)sender {
    
    
    
}

- (void) drawTimer {
    
    // Set up the shape of the circle
    int radius = (timerView.frame.size.width/2)+20;
    CAShapeLayer *circle = [CAShapeLayer layer];
    
    // Make a circular shape
    circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                             cornerRadius:radius].CGPath;
    // Center the shape in self.view
    circle.position = CGPointMake(CGRectGetMidX(timerView.frame)-radius,
                                  CGRectGetMidY(timerView.frame)-radius);
    
    // Configure the apperence of the circle
    circle.fillColor = [UIColor clearColor].CGColor;
    circle.strokeColor = [UIColor whiteColor].CGColor;//[UIColor colorWithRed:85.0f/255.0f green:190.0f/255.0f blue:50.0f/255.0f alpha:1.0f].CGColor;
    circle.lineWidth = 10;
    
    // Add to parent layer
    [step2.layer addSublayer:circle];
    
    // Configure animation
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = 2.0; // "animate over 10 seconds or so.."
    drawAnimation.repeatCount         = 1.0;  // Animate only once..
    
    // Animate from no part of the stroke being drawn to the entire stroke being drawn
    drawAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    drawAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
    
    // Experiment with timing to get the appearence to look the way you want
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    // Add the animation to the circle
    [circle addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
    
    //[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(successView) userInfo:nil repeats:NO];
    
}

- (void) drawSuccess {
    
    remindMe.hidden = NO;
    remindMe.alpha = 0.0f;
    
    successView.alpha = 1.0;
    successView.hidden = NO;
    
    successView.transform = CGAffineTransformMakeScale(0.0, 0.0);
    
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        
        remindMe.alpha = 1.0;
        successView.alpha = 1.0;
        successView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        
    } completion: ^(BOOL complete) {
        
        [UIView animateWithDuration:0.5f delay:3.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            
            successView.alpha = 0.0;
            successView.transform = CGAffineTransformMakeScale(1.1, 1.1);
            
        } completion: ^(BOOL complete) {
            
            [self performSelector:@selector(drawSuccess) withObject:nil afterDelay:0.1];
            
        }];
        
    }];
    
}

- (void) drawFailure {
    
    failView.alpha = 1.0;
    failView.hidden = NO;
    
    failView.transform = CGAffineTransformMakeScale(0.0, 0.0);
    
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        
        failView.alpha = 1.0;
        failView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        
    } completion: ^(BOOL complete) {
        
        [UIView animateWithDuration:0.5f delay:3.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            
            failView.alpha = 0.0;
            failView.transform = CGAffineTransformMakeScale(1.1, 1.1);
            
        } completion: ^(BOOL complete) {
            
            [self performSelector:@selector(drawFailure) withObject:nil afterDelay:0.1];
            
        }];
        
    }];
    
}

- (IBAction)cancelButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) successView {
    
    [self nextStep];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
 
    appDelegate.currentRFID = [RFIDInput.text mutableCopy];
    
    RFIDInput.text = @"";
    
    [self.view endEditing:YES];
    
    // load next step
    
    [self nextStep];
    
    return YES;
    
}

- (void) nextStep {
    
    current_step++;
    
    if(current_step <= [steps count]){
        
        UIView *currentStep;
        
        if(current_step == 2 && KEY_USED)
            currentStep = [steps objectAtIndex:3];
        else
            currentStep = [steps objectAtIndex:current_step];
        
        UIView *previousStep = [steps objectAtIndex:current_step-1];
        
        currentStep.hidden = NO;
        previousStep.hidden = NO;
        
        currentStep.alpha = 0.0f;
        
        [currentStep setFrame:CGRectMake(currentStep.frame.origin.x, self.view.frame.size.height, currentStep.frame.size.width, currentStep.frame.size.height)];
        
        [UIView animateWithDuration:1.0f animations:^{
           
            currentStep.alpha = 1.0f;
            previousStep.alpha = 0.0f;
            
            [currentStep setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
            [previousStep setFrame:CGRectMake(currentStep.frame.origin.x, currentStep.frame.size.height*-1, currentStep.frame.size.width, currentStep.frame.size.height)];
            
        } completion:^(BOOL finished) {
            
            if(current_step == 1) {
            
                [self drawTimer];
                
                if(appDelegate.internetIsReachable) {
                
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                        
                        NSData* data = [NSData dataWithContentsOfURL:
                                        [NSURL URLWithString:[NSString stringWithFormat:@"http://register.growtix.com/mobile/checkFastPassEligibility/846/%@/%@", appDelegate.currentPanel, appDelegate.currentRFID]]];
                        
                        NSLog(@"http://register.growtix.com/mobile/checkFastPassEligibility/846/%@/%@", appDelegate.currentPanel, appDelegate.currentRFID);
                        
                        //NSLog(@"http://register.growtix.com/mobile/checkFastPassEligibility/158/%@/%@", appDelegate.currentPanel, appDelegate.currentRFID);
                        
                        [self performSelectorOnMainThread:@selector(parseResults:)
                                               withObject:data waitUntilDone:YES];
                    });
                    
                } else {
                    
                    [appDelegate noInternet];
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }
                
            } else if(current_step == 2) {
                
                if(KEY_USED) {
                    
                    [self drawFailure];
                    [cancelButton setTitle:@"Try Another Panel" forState:UIControlStateNormal];
                    
                } else {
                
                    [self drawSuccess];
                    [cancelButton setTitle:@"Done" forState:UIControlStateNormal];
                    
                }
            }
            
            previousStep.hidden = YES;
            
        }];
        
    }
    
}

- (void) parseResults: (NSData *) data {
    
    NSError* error;
    
    NSMutableArray *result = [NSJSONSerialization
              JSONObjectWithData:data //1
              
              options:kNilOptions
              error:&error];

    NSMutableString *result_value = [result objectAtIndex:0];
    
    NSString *queue_limit = @"";
    
    if(result.count > 1)
        queue_limit = [result objectAtIndex:1];
    
    switch ([result_value integerValue]) {
        case 0:
        {
            // already in queue
            
            KEY_USED = YES;
            [failText setText:@"Sorry, you are already in queue for this panel."];
            [self nextStep];
            
        }
            break;
        case 1:
        {
            // panel not valid
            
            KEY_USED = YES;
            [failText setText:@"Sorry, this panel is not eligible for Fast Pass."];
            [self nextStep];
            
        }
            break;
        case 2:
        {
            // at capacity
            
            KEY_USED = YES;
            [failText setText:@"Sorry, this panel is at capacity."];
            [self nextStep];
            
        }
            break;
        case 3:
        {
            // added to queue!
            
            KEY_USED = NO;
            
            [self nextStep];
            
        }
            break;
        case 4:
        {
            // at capacity
            
            KEY_USED = YES;
            [failText setText:@"Sorry, your pass type is not eligible for reserving this panel."];
            [self nextStep];
            
        }
            break;
        case 5:
        {
            // at capacity
            
            KEY_USED = YES;
            
            NSString *panelPlural = @"Panels";
            
            if([queue_limit intValue]<2)
                panelPlural = @"Panel";
            
            [failText setText:[NSString stringWithFormat:@"Sorry, you have reached the queue limit for your pass.\n(%@) %@.",queue_limit,panelPlural]];
            [self nextStep];
            
        }
            break;
            
        default:
            break;
    }
    
}

- (IBAction)setReminder:(id)sender {
    
    UIStoryboard *storyboard = self.storyboard;
    ReminderViewController *firstView = [storyboard instantiateViewControllerWithIdentifier:@"reminder_view"];
    
    [firstView setPanel_id:appDelegate.currentPanel];
    
    [self.navigationController pushViewController:firstView animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
