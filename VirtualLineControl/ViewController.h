//
//  ViewController.h
//  VirtualLineControl
//
//  Created by John Sloan on 8/17/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableArray *panels;
    __weak IBOutlet UITableView *mainTable;
    __weak IBOutlet UIButton *managePanels;
    AppDelegate *appDelegate;
    __weak IBOutlet UILabel *noPanels;
    
}

- (void) getPanels;

@end

