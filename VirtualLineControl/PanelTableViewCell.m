//
//  PanelTableViewCell.m
//  VirtualLineControl
//
//  Created by John Sloan on 8/17/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import "PanelTableViewCell.h"

@implementation PanelTableViewCell

@synthesize title, VIPONLY, pass_types, remaining, panelTime;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
