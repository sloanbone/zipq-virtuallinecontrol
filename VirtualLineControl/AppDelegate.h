//
//  AppDelegate.h
//  VirtualLineControl
//
//  Created by John Sloan on 8/17/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Reachability;

@class ViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate> {
    
    Reachability* internetReachable;
    Reachability* hostReachable;
    
    BOOL internetIsReachable;
    
    NSMutableArray *usedKeys;
    NSMutableString *currentPanel;
    NSMutableString *currentRFID;
    
    ViewController *mainViewController;
    
    UIAlertView *a;
    BOOL alert_visible;
    
}

@property (nonatomic, assign) BOOL internetIsReachable;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSMutableArray *usedKeys;
@property (nonatomic, strong) NSMutableString *currentPanel;
@property (nonatomic, strong) NSMutableString *currentRFID;

@property (nonatomic, strong) ViewController *mainViewController;

- (void) noInternet;

@end

