//
//  ManageViewController.h
//  VirtualLineControl
//
//  Created by John Sloan on 9/1/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@interface ManageViewController : UIViewController <UITextFieldDelegate> {
    
    IBOutlet UIButton *cancelButton;
    IBOutlet UITextField *rfidInput;
    
    UIView *pulse;
    
    __weak IBOutlet UIView *step1;
    __weak IBOutlet UIView *step2;
    __weak IBOutlet UIView *step3;
    __weak IBOutlet UIView *failView;
    __weak IBOutlet UIView *timerView;
    __weak IBOutlet UIView *beaconPulse;
    
    AppDelegate *appDelegate;
    
    NSMutableArray *steps;
    int current_step;
    
    BOOL KEY_USED;
    
}


@end
