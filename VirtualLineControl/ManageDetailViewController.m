//
//  ManageDetailViewController.m
//  VirtualLineControl
//
//  Created by John Sloan on 9/1/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import "ManageDetailViewController.h"
#import "PanelTableViewCell.h"
#import "AppDelegate.h"

@interface ManageDetailViewController ()

@end

@implementation ManageDetailViewController

@synthesize user_panels;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    backButton.layer.cornerRadius = 5;
    backButton.layer.borderColor = [UIColor whiteColor].CGColor;
    backButton.layer.borderWidth = 1;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goHome:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // options cell
    
    static NSString *CellIdentifier = @"OptionCell";
    
    PanelTableViewCell *cell = (PanelTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            NSArray *arrayCellXib = [[NSBundle mainBundle] loadNibNamed:@"PanelTableViewCell"
                                                                  owner:self
                                                                options:nil];
            cell = [arrayCellXib objectAtIndex:0];
            
        } else {
            
            NSArray *arrayCellXib = [[NSBundle mainBundle] loadNibNamed:@"PanelTableViewCell"
                                                                  owner:self
                                                                options:nil];
            cell = [arrayCellXib objectAtIndex:0];
            
        }
        
    }
    
    cell.title.text = [[[user_panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"title"];
    cell.panelTime.text = [[[user_panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"time"];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    removal = [[[user_panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"id"];
    
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Confirmation" message:@"Are you sure you want to be removed from this queue? You will lose your place and if the panel is full you will not be able to enter the queue again." delegate:self cancelButtonTitle:@"Yes, remove me" otherButtonTitles:@"No, Don't remove me", nil];
    
    [a show];
    
    /*appDelegate.currentPanel = [[[panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"id"];
    
    UIStoryboard *storyboard = self.storyboard;
    TapViewController *firstView = [storyboard instantiateViewControllerWithIdentifier:@"tap_view"];
    [self.navigationController pushViewController:firstView animated:YES];*/
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0) {
        
        if(appDelegate.internetIsReachable) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            NSData* data = [NSData dataWithContentsOfURL:
                            [NSURL URLWithString:[NSString stringWithFormat:@"http://register.growtix.com/mobile/toggleFastPass/846/%@/%@", removal, appDelegate.currentRFID]]];
            
            [self performSelectorOnMainThread:@selector(parseResults:)
                                   withObject:data waitUntilDone:YES];
        
        });
            
        } else {
            
            [appDelegate noInternet];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
        
    }
    
}

- (void) parseResults: (NSData *) data {
    
    NSError* error;

    user_panels = [NSJSONSerialization
                              JSONObjectWithData:data //1
                              
                              options:kNilOptions
                              error:&error];
    
    if([user_panels count] < 1) {
        
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"No Reservations Left" message:@"You are no longer queued for any panels." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [a show];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    } else {
        
        [mainTable reloadData];
        [mainTable reloadInputViews];
        
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [user_panels count];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
