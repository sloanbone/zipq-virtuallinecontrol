//
//  PanelTableViewCell.h
//  VirtualLineControl
//
//  Created by John Sloan on 8/17/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PanelTableViewCell : UITableViewCell {
    
    IBOutlet UILabel *title;
    IBOutlet UIImageView *VIPONLY;
    IBOutlet UILabel *remaining;
    IBOutlet UILabel *pass_types;
    IBOutlet UILabel *panelTime;
    
}

@property (nonatomic, strong) IBOutlet UILabel *title;
@property (nonatomic, strong) IBOutlet UIImageView *VIPONLY;
@property (nonatomic, strong) IBOutlet UILabel *remaining;
@property (nonatomic, strong) IBOutlet UILabel *pass_types;
@property (nonatomic, strong) IBOutlet UILabel *panelTime;

@end
