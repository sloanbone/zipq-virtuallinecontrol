//
//  ViewController.m
//  VirtualLineControl
//
//  Created by John Sloan on 8/17/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import "ViewController.h"
#import "PanelTableViewCell.h"
#import "TapViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    managePanels.layer.cornerRadius = 5;
    managePanels.layer.borderColor = [UIColor whiteColor].CGColor;
    managePanels.layer.borderWidth = 1;
    
    appDelegate.mainViewController = self;
    
}

- (void) getPanels {
    
    if(appDelegate.internetIsReachable) {
    
        NSLog(@"GET PANELS");
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            NSData* data = [NSData dataWithContentsOfURL:
                            [NSURL URLWithString:[NSString stringWithFormat:@"https://register.growtix.com/mobile/getFastPassPanels/3396"]]];
            [self performSelectorOnMainThread:@selector(parsePanels:)
                                   withObject:data waitUntilDone:YES];
        });
        
    } else {
        
       // [appDelegate noInternet];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
    
            [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(getPanels) userInfo:nil repeats:NO];
            
        });
        
        //[[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        
    }
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [self getPanels];
    
}

- (void) parsePanels:(NSData *) responseData {
    
    NSError* error;
    
    panels = [NSJSONSerialization
               JSONObjectWithData:responseData //1
               
               options:kNilOptions
               error:&error];
    
    if([panels count] < 1)
        [noPanels setHidden:NO];
    else
        [noPanels setHidden:YES];
    
    [mainTable reloadData];
    [mainTable reloadInputViews];
    
    [NSTimer scheduledTimerWithTimeInterval:25.0 target:self selector:@selector(getPanels) userInfo:nil repeats:NO];
    //[[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // options cell
    
    static NSString *CellIdentifier = @"OptionCell";
    
    PanelTableViewCell *cell = (PanelTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            NSArray *arrayCellXib = [[NSBundle mainBundle] loadNibNamed:@"PanelTableViewCell"
                                                                  owner:self
                                                                options:nil];
            cell = [arrayCellXib objectAtIndex:0];
            
        } else {
            
            NSArray *arrayCellXib = [[NSBundle mainBundle] loadNibNamed:@"PanelTableViewCell"
                                                                  owner:self
                                                                options:nil];
            cell = [arrayCellXib objectAtIndex:0];
            
        }
        
    }
    
    if([[[[panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"fast_pass_vip_only"] boolValue])
        cell.VIPONLY.hidden = NO;
    
    cell.remaining.hidden = NO;
    cell.pass_types.hidden = NO;
    
    cell.title.text = [[[panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"title"];
    cell.remaining.text = [[[panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"description"];
    cell.pass_types.text = [[[panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"eligable"];
    cell.panelTime.text = [[[panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"time"];
    //cell.panelTime.text = @"TEST";
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 101;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if([[[[panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"description"] isEqualToString:@"FULL"])
    {
        
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Panel Full" message:@"Sorry, this panel has filled up." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [a show];
        
        return;
        
    }
    
    appDelegate.currentPanel = [[[panels objectAtIndex:indexPath.row] objectForKey:@"Schedule"] objectForKey:@"id"];
    
    UIStoryboard *storyboard = self.storyboard;
    TapViewController *firstView = [storyboard instantiateViewControllerWithIdentifier:@"tap_view"];
    [self.navigationController pushViewController:firstView animated:YES];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [panels count];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
