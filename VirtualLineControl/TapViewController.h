//
//  TapViewController.h
//  VirtualLineControl
//
//  Created by John Sloan on 8/17/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@interface TapViewController : UIViewController <UITextFieldDelegate> {
    
    __weak IBOutlet UIView *beaconPulse;
    UIView *pulse;
    
    UIView *countDown;
    
    __weak IBOutlet UIButton *cancelButton;
    
    __weak IBOutlet UIView *step1;
    __weak IBOutlet UIView *step2;
    __weak IBOutlet UIView *step3;
    __weak IBOutlet UIView *step4;
    
    __weak IBOutlet UITextField *RFIDInput;
    NSMutableArray *steps;
    int current_step;
    __weak IBOutlet UIView *timerView;
    __weak IBOutlet UIView *successView;
    __weak IBOutlet UIView *failView;
    __weak IBOutlet UILabel *failText;
    
    AppDelegate *appDelegate;
    
    BOOL KEY_USED;
    __weak IBOutlet UIButton *remindMe;
    
}

@end
