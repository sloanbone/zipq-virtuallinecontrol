//
//  ManageDetailViewController.h
//  VirtualLineControl
//
//  Created by John Sloan on 9/1/15.
//  Copyright (c) 2015 MediaOne of Utah. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@interface ManageDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate> {
    
    NSMutableArray *user_panels;
    IBOutlet UIButton *backButton;
    __weak IBOutlet UITableView *mainTable;
    
    NSMutableString *removal;
    
    AppDelegate *appDelegate;
    
}

@property (nonatomic, strong) NSMutableArray *user_panels;

@end
